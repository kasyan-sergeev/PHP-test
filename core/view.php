<?php

class View {

    public function __construct() {
        
    }

    public function render($name = false) {
        require_once 'views/template_header.php';
        require_once 'views/v_' . $name . '.php';
        require_once 'views/template_footer.php';
        exit();
    }

    public function authentication() {
        require_once 'views/v_login.php';
        exit();
    }

}

?>