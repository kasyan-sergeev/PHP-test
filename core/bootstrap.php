<?php

class Bootstrap {

    public static function checkAuthentication() {
        require_once 'controllers/c_authentication.php';
        require_once 'models/m_authentication.php';
        M_Authentication::check();
    }

    public static function route() {
        $url = explode('/', rtrim($_GET['route'], '/'));

        $controller_path = 'controllers/c_' . $url[0] . '.php';
        $model_path = 'models/m_' . $url[0] . '.php';

        if (empty($url[0])) {
            $url[0] = 'index';
        } elseif (!file_exists($controller_path) && !file_exists($model_path)) {
            $url[0] = 'error';
        }

        $controller_path = 'controllers/c_' . $url[0] . '.php';
        $model_path = 'models/m_' . $url[0] . '.php';

        require_once "$controller_path";
        require_once "$model_path";

        $controller_name = 'c_' . $url[0];
        $controller = new $controller_name();

        $model_name = 'm_' . $url[0];
        $model = new $model_name();

        if (!empty($url[1])) {
            if (!empty($url[2]) && !empty($url[3])) {
                $model->{$url[1]}($url[2], $url[3]);
            } elseif (!empty($url[2]) && empty($url[3])) {
                $model->{$url[1]}($url[2]);
            } else {
                $model->{$url[1]}();
            }
        }

        $controller->view->render($url[0]);
    }

}

?>