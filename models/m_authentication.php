<?php

class M_Authentication extends Model {

    public static $pdo;
    private static $login;
    private static $password;

    public static function check() {
        if (empty($_SESSION['login']) && empty($_SESSION['password'])) {
            if (empty($_POST['submit'])) {

                $controller = new C_Authentication();
                echo 'Login through the login page.';
                $controller->view->authentication();
            } elseif (empty($_POST['login']) || empty($_POST['password'])) {
                $controller = new C_Authentication();
                echo 'Enter your login and password.';
                $controller->view->authentication();
            } else {
                self::$login = $_POST['login'];
                self::$password = $_POST['password'];

                $_SESSION['login'] = self::$login;
                $_SESSION['password'] = self::$password;

                self::connect(self::$login, self::$password);
            }
        } else {
            self::$login = $_SESSION['login'];
            self::$password = $_SESSION['password'];

            self::connect(self::$login, self::$password);
        }
    }

    private function connect($login, $password) {
        try {
            self::$pdo = new PDO('mysql:host=localhost;dbname=newBase', $login, $password);
            self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$pdo->exec('SET NAMES "utf8"');
            self::$pdo->query('SELECT id,login FROM users');
        } catch (PDOException $e) {
            $controller = new C_Authentication();
            echo 'Wrong login or password.'.$e;
            $controller->view->authentication();
        }

        Bootstrap::route();
    }

}

?>