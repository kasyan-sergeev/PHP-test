<?php

class M_Userlist extends Model {

    //for __construct
    public static $ids;
    public static $logins;
    //for savevalues
    public static $id;
    public static $login;
    public static $password;
    public static $password2;
    public static $about;
    public static $name;
    public static $surname;
    public static $gender;
    //for save and delete
    public static $add;
    public static $sql;
    //for pagination
    public static $page;
    public static $next;
    public static $prev;
    public static $lastPage;
    public static $start;
    public static $idsInThisPage;
    public static $loginsInThisPage;

    public function pagination($page = 1) {
        self::$lastPage = ceil(count(self::$ids) / 2);
        if ($page <= 0 || $page > self::$lastPage) {
            self::$page = 1;
        } else {
            self::$page = $page;
        }
        self::$next = self::$page + 1;
        self::$prev = self::$page - 1;

        self::$start = (self::$page - 1) * 2;

        try {
            $users = M_Authentication::$pdo->query("SELECT `id`,`login` FROM users LIMIT " . self::$start . " , 2");
        } catch (PDOException $e) {
            exit('Can\'t select from the database' . $return_to_main . $e);
        }

        foreach ($users as $row) {
            self::$idsInThisPage[] = $row['id'];
            self::$loginsInThisPage[$row['id']] = $row['login'];
        }
    }

    function __construct() {
        try {
            $users = M_Authentication::$pdo->query('SELECT id,login FROM users');
        } catch (PDOException $e) {
            exit('Can\'t select from the database');
        }

        foreach ($users as $row) {
            self::$ids[] = $row['id'];
            self::$logins[$row['id']] = $row['login'];
        }
        self::pagination($_POST['page']);
    }

    private static function savevalues() {
        self::$id = $_POST['id'];
        self::$login = $_POST['login'];
        self::$password = $_POST['password'];
        self::$password2 = $_POST['password2'];
        self::$name = $_POST['name'];
        self::$surname = $_POST['surname'];
        self::$gender = $_POST['gender'];
    }

    private function saveEditInDB() {
        self::$about = json_encode(array('name' => self::$name, 'surname' => self::$surname, 'gender' => self::$gender));
        try {
            $sql = "UPDATE `users` SET `login`=:login,`password`=:password,`about`=:about WHERE `id`=:id";
            $add = M_Authentication::$pdo->prepare($sql);
            $add->bindValue(':login', self::$login);
            $add->bindValue(':password', self::$password);
            $add->bindValue(':id', self::$id);
            $add->bindValue(':about', self::$about);
            $add->execute();
        } catch (PDOException $e) {
            exit('Some errors');
        }
    }

    private function query($id) {
        try {
            $user = M_Authentication::$pdo->query("SELECT * FROM users WHERE `id` = '$id'");
        } catch (PDOException $e) {
            exit($e);
        }

        foreach ($user as $row) {
            self::$id = $row['id'];
            self::$login = $row['login'];
            self::$password = $row['password'];
            self::$about = $row['about'];
        }
        self::$password2 = self::$password;

        self::$about = json_decode(self::$about);

        self::$name = self::$about->{'name'};
        self::$surname = self::$about->{'surname'};
        self::$gender = self::$about->{'gender'};
    }

    public function info($id) {
        if (!isset($id)) {
            exit('Some errors');
        }
        $this->query($id);

        $controller = new C_Userlist();
        $controller->view->render('info');
    }

    public function edit($id) {
        M_Userlist::savevalues();

        if (!isset($id)) {
            exit('Some errors');
        }

        if (isset($_POST[submit])) {
            try {
                if (empty($_POST['login']) || empty($_POST['password']) || empty($_POST['password2']) || empty($_POST['name']) || empty($_POST['surname']) || empty($_POST['gender'])) {
                    throw new Exception('One of input is empty');
                }
                if ($_POST['password'] !== $_POST['password2']) {
                    throw new Exception('Passwords don\'t match');
                }

                if (self::$logins[$_POST['id']] != $_POST['login']) {
                    foreach (self::$logins as $key) {
                        if ($_POST['login'] == $key) {
                            throw new Exception('This login is busy');
                        } else {
                            self::$login = $_POST['login'];
                        }
                    }
                }
            } catch (Exception $e) {
                echo $e->getMessage();
                $controller = new C_Userlist ();
                $controller->view->render('edit');
            }
            M_Userlist::saveEditInDB();
        } else {
            $this->query($id);
        }
        $controller = new C_Userlist ();
        $controller->view->render('edit');
    }

    public function delete($id) {

        if (!isset($id)) {
            exit('Some errors');
        } else {
            try {
                self::$sql = "DELETE FROM `users` WHERE `id` = :id";
                self::$add = M_Authentication::$pdo->prepare(self::$sql);
                self::$add->bindValue(':id', $id);
                self::$add->execute();
                echo fdsfs;
            } catch (PDOException $e) {
                exit('Some errors');
            }
        }
        $controller = new C_Userlist();
        $controller->view->render('delete');
    }

}

?>