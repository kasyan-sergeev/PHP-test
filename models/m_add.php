<?php

class M_Add {

    public static $ids;
    public static $logins;
    public static $login;
    public static $password;
    public static $password2;
    public static $about;
    public static $name;
    public static $surname;
    public static $gender;

    private static function savevalues() {
        self::$login = $_POST['login'];
        self::$password = $_POST['password'];
        self::$password2 = $_POST['password2'];
        self::$name = $_POST['name'];
        self::$surname = $_POST['surname'];
        self::$gender = $_POST['gender'];
    }

    private function saveInDB() {
        self::$about = json_encode(array('name' => self::$name, 'surname' => self::$surname, 'gender' => self::$gender));
        try {
            $sql = "INSERT INTO `users` (`id`, `login`, `password`, `about`) VALUES (NULL, :login, :password, :about)";
            $add = M_Authentication::$pdo->prepare($sql);
            $add->bindValue(':login', self::$login);
            $add->bindValue(':password', self::$password);
            $add->bindValue(':about', self::$about);
            $add->execute();
        } catch (PDOException $e) {
            echo 'Some errors<br>' . $e;
        }
    }

    function __construct() {
        try {
            $users = M_Authentication::$pdo->query('SELECT id,login FROM users');
        } catch (PDOException $e) {
            exit('Can\'t select from the database');
        }

        foreach ($users as $row) {
            self::$ids[] = $row['id'];
            self::$logins[$row['id']] = $row['login'];
        }

        self::savevalues();
        if (isset($_POST['submit'])) {
            try {
                if (empty($_POST['login']) || empty($_POST['password']) || empty($_POST['password2']) || empty($_POST['name']) || empty($_POST['surname']) || empty($_POST['gender'])) {
                    throw new Exception('One of input is empty');
                }
                if ($_POST['password'] !== $_POST['password2']) {
                    throw new Exception('Passwords don\'t match');
                }

                self::$password2 = self::$password;

                foreach (self::$logins as $key) {
                    if ($_POST['login'] == $key) {
                        throw new Exception('This login is busy');
                    }
                }
            } catch (Exception $e) {
                echo $e->getMessage();
                $controller = new C_Add();
                $controller->view->render('add');
            }
            self::saveInDB();
        }
    }

}

?>